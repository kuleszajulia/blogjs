const express = require('express'), // import frameworka express
	app = express(), // stworzenie instancji aplikacji 
	port = 8080, // ustawienie portu aplikacji na 8080
	mongoose = require('mongoose'), //import biblio do obsługio bazy mongoDB
	bodyParser = require('body-parser'),
	BlogPosts = require('./api/models/blogModel'), //import modelu blogpostu (tytul, text, data mod, i data utworzenia)
	Users = require('./api/models/userModel'); //import modelu uzytkownia (nazwa, mail, haslo)

// mongoose instance connection url connection
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/BlogDB', {useNewUrlParser: true, useUnifiedTopology: true}) //połaczeni do bazy danych mongoDB
	.then(() => console.log("Connected to the MongoDB")) // jak sukces to napisz że połączono
	.catch(err => console.log(err)); // jesli nie to wypisz błąd 

app.use(bodyParser.urlencoded({ extended: true })); 
app.use(bodyParser.json());

//require = mport 
require('./api/routes/authRoutes')(app); //rejestrowanie endpointów związanych z autpryzacją (używam tokenu - autoryzuje się) i uwierzytelnianiem (cza ja to ja) log -dostaje token
require('./api/routes/blogRoutes')(app); // rejestracja endpointów związanych z blogpostami

app.listen(port, () => { //uruchomienie serwerana porcie 8080
	console.log('Blog API server started on: ' + port);
});