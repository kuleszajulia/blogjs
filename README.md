API server umożliwiający rejestrację oraz odczyt blog postów z wykorzystaniem Node.js i MongoDB.

# Wymagania

- [Node.js + npm](https://nodejs.org/en/)
- [MongoDB](https://www.mongodb.com/download-center/community)

# Uruchomienie

Instalacja zależności:
```npm install```

Uruchomienie serwera:
```npm run start```

# API

```
[POST] /register
[POST] /login
[POST] /logout
[GET, POST] /blog_posts
[GET, PUT, DELETE] /blog_posts/:id
```

# Na podstawie

- [RESTful API](https://www.codementor.io/@olatundegaruba/nodejs-restful-apis-in-10-minutes-q0sgsfhbd)
- [JSON Web Tokens](https://www.freecodecamp.org/news/securing-node-js-restful-apis-with-json-web-tokens-9f811a92bb52/)
- [Secure rest API](https://www.toptal.com/nodejs/secure-rest-api-in-nodejs)