'use strict';
module.exports = function(app) {
  const blogPosts = require('../controllers/blogController');
  const auth = require('../controllers/authController');

  app.route('/blog_posts')
      .all(auth.hasPermission)
      .get(blogPosts.list_all_blog_posts)
      .post(blogPosts.create_blog_post);

  app.route('/blog_posts/:blogPostId')
      .all(auth.hasPermission)
      .get(blogPosts.read_blog_post)
      .put(blogPosts.update_blog_post)
      .delete(blogPosts.delete_blog_post);
};