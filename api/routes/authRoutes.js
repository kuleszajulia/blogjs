'use strict';

module.exports = function(app) {
    const auth = require('../controllers/authController');

    app.route('/register')
        .post(auth.register_new_user);

    app.route('/login')
        .post(auth.login);

    app.route('/logout')
        .post(auth.logout);

};