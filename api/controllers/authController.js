const jwt = require('jsonwebtoken'),
    bcrypt = require('bcryptjs'),
    config = require('../../config'),
    mongoose = require('mongoose'),
    Users = mongoose.model('Users'),
    tokenLiveTime = 24*60*60; // expires in 24 hours

exports.register_new_user = function(req, res) {
    const hashedPassword = bcrypt.hashSync(req.body.password, 8);
    Users.create({
            name : req.body.name,
            email : req.body.email,
            password : hashedPassword
        },
        function (err, user) {
            if (err)
                return res.status(500).send("There was a problem registering the user.");
            // create a token
            const token = jwt.sign({ id: user._id }, config.secret, {
                expiresIn: tokenLiveTime
            });
            res.status(200).send({ auth: true, token: token });
        });
};

exports.login = function(req, res) {
    Users.findOne({ email: req.body.email }, function (err, user) {
        if (err)
            return res.status(500).send('Error on the server.');
        if (!user)
            return res.status(404).send('No user found.');

        const passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
        if (!passwordIsValid)
            return res.status(401).send({ auth: false, token: null });

        const token = jwt.sign({ id: user._id }, config.secret, {
            expiresIn: tokenLiveTime
        });

        res.status(200).send({ auth: true, token: token });
    });
};

exports.logout = function(req, res) {
    res.status(200).send({ auth: false, token: null });
};

exports.hasPermission = function(req, res, next) {
    const token = req.headers['x-access-token'];
    if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });

    jwt.verify(token, config.secret, function(err, decoded) {
        if (err)
            return res.status(500).send({auth: false, message: 'Failed to authenticate token.'});
        next();
    });
};