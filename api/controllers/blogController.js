'use strict';
const mongoose = require('mongoose'), // import biblioteki obslugi bazy danych
    BlogPosts = mongoose.model('BlogPosts'); //import modelu blogpostu z bazy danych

exports.list_all_blog_posts = function(req, res) { // exports - udostepnia metode na zewnatrz pliku
  BlogPosts.find({}, function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};

exports.create_blog_post = function(req, res) {
  let blog_post = new BlogPosts(req.body);
  blog_post.save(function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};

exports.read_blog_post = function(req, res) {
  BlogPosts.findById(req.params.blogPostId, function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};

exports.update_blog_post = function(req, res) {
  req.body.last_modified = Date.now();
  BlogPosts.findOneAndUpdate(
      {_id: req.params.blogPostId},
      req.body,
      {new: true},
      function(err, task) {
        if (err)
          res.send(err);
        res.json(task);
      });
};

exports.delete_blog_post = function(req, res) {
  BlogPosts.remove({
    _id: req.params.blogPostId
  }, function(err, task) {
    if (err)
      res.send(err);
    res.json({ message: 'Blog post successfully deleted' });
  });
};