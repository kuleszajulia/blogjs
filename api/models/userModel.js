
'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    name: {
        type: String,
        required: 'Enter your name'
    },
    email: {
        type: String,
        required: 'Enter your email'
    },
    password: {
        type: String,
        required: 'Enter password'
    }
});

module.exports = mongoose.model('Users', UserSchema);