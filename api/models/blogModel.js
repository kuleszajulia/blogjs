'use strict';
const mongoose = require('mongoose'); //import biblio
const Schema = mongoose.Schema; //nowy obiekt, dzieki temu baza danych bedzie w stanie stworzyć tabele i zrobić mapowanie 

const BlogPostSchema = new Schema({
    title: {
        type: String,
        required: 'Enter the title of your blog post' // jest wymagany 
    },
    text: {
        type: String,
        required: 'Enter text of your blog post'
    },
    created_date: {
        type: Date,
        default: Date.now
    },
    last_modified: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('BlogPosts', BlogPostSchema); // export dzieki czemu bedzie dostepny w calej aplikacji 